-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 19 Tem 2018, 16:27:22
-- Sunucu sürümü: 10.1.31-MariaDB
-- PHP Sürümü: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `atacak`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pricelist`
--

CREATE TABLE `pricelist` (
  `id` int(11) NOT NULL,
  `stok_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `pp` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `ep` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `cu` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `productcatalog`
--

CREATE TABLE `productcatalog` (
  `id` int(11) NOT NULL,
  `kategori_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `d1` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `d2` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `l1` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `l2` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `l3` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `br` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `kdv` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `img` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `ean` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `atp` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `dm3` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `ozelikler` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `productcategories`
--

CREATE TABLE `productcategories` (
  `id` int(11) NOT NULL,
  `katalog_id` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `D` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `productstocks`
--

CREATE TABLE `productstocks` (
  `id` int(11) NOT NULL,
  `stok_ID` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `ATP` varchar(250) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `pricelist`
--
ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `productcatalog`
--
ALTER TABLE `productcatalog`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `productcategories`
--
ALTER TABLE `productcategories`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `productstocks`
--
ALTER TABLE `productstocks`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `pricelist`
--
ALTER TABLE `pricelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `productcatalog`
--
ALTER TABLE `productcatalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `productcategories`
--
ALTER TABLE `productcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `productstocks`
--
ALTER TABLE `productstocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
